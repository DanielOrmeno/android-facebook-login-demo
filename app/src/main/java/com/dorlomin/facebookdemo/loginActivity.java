package com.dorlomin.facebookdemo;

/***************************************************************************************************
 |   File: loginActivity.java
 |   Proyect: Android - Facebook Login Demo
 |
 |   Description: - Login Activity, verifies validity of the current session (AccessToken), requests
 |  login if session is invalid, if the sesion is valid fetches user's liked pages and starts new
 |  activity,
 |
 |   Copyright (c) AppFactory. All rights reserved.
 **************************************************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

public class loginActivity extends ActionBarActivity {

    //------------------------------- CLASS VARIABLES --------------------------------------------//

    //- UI VIEWS
    private View loginPanel;
    private View loadingPanel;

    //- UI elements
    private LoginButton facebookLoginButton;
    private TextView statusLabel;

    //- Class Variables
    private CallbackManager callbackManager;
    private AccessToken accessToken;

    //- API TAGS
    private static String LIKES_TAG = "likes";
    private static String DATA_TAG = "data";

    //- Intent TAGS
    private static String LIKEDPAGES_TAG = "LIKEDPAGES";

    //------------------------------- ACTIVITY SPECIFIC METHODS ----------------------------------//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //- Initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());

        //- Callback Manager
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        //- Reference to UI Viwes
        loginPanel = findViewById(R.id.login_layout);
        loadingPanel = findViewById(R.id.spinner_layout);

        //- Reference to UI elements
        statusLabel = (TextView) findViewById(R.id.status_label);
        facebookLoginButton = (LoginButton) findViewById(R.id.login_button);
        facebookLoginButton.setReadPermissions("user_likes");

        //- Get current AccessToken
        accessToken = AccessToken.getCurrentAccessToken();

        //- Verify facebook session, if valid fetch relevant data, if not request login.
        //- If AccessToken is valid make API call and show ListActivity.
        if (accessToken == null) {
            loadingPanel.setVisibility(View.GONE);
            loginPanel.setVisibility(View.VISIBLE);
            Log.i("Login", "AccessToken is null");

        } else if (!accessToken.isExpired()){
            requestUserLikedPages(accessToken);
            loadingPanel.setVisibility(View.GONE);
            loginPanel.setVisibility(View.VISIBLE);
            Log.i("Login", "AccessToken is valid");
        } else {
            loadingPanel.setVisibility(View.GONE);
            loginPanel.setVisibility(View.VISIBLE);
            Log.i("Login", "AccessToken is invalid");
        }

        //- Facebook Login Callback declaration
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                statusLabel.setText("Success");
                Log.i ("Login", "Login Successful");
                accessToken = loginResult.getAccessToken();

                //- Perform API call
                requestUserLikedPages(accessToken);
            }

            @Override
            public void onCancel() {
                statusLabel.setText("Canceled");
            }

            @Override
            public void onError(FacebookException e) {
                statusLabel.setText("Failed");
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //------------------------------- CLASS METHODS ----------------------------------------------//

    private void requestUserLikedPages ( AccessToken accessToken) {

        //- Graph API Call

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            //- get JSONObject from GraphResponse
                            JSONObject graphData = response.getJSONObject().getJSONObject(LIKES_TAG);

                            Intent listIntent = new Intent(loginActivity.this, ListActivity.class);

                            //- Add JSONObject "data" as String.
                            listIntent.putExtra(LIKEDPAGES_TAG, graphData.toString());

                            //- Login is successful, Switch to List Activity.
                            startActivity(listIntent);
                        } catch (JSONException e ) {
                            e.printStackTrace();
                        }
                    }
                });

        //- API call parameters / - Include user liked pages.
        Bundle parameters = new Bundle();
        parameters.putString("fields", LIKES_TAG);
        request.setParameters(parameters);
        //- Perform API call asynchronously
        request.executeAsync();
    }
}
