package com.dorlomin.facebookdemo;

/***************************************************************************************************
 |   File: UserLikedPage.java
 |   Proyect: Android - Facebook Login Demo
 |
 |   Description: - User Liked Page class to objectify response from API Call - Used to populate
 |   list view in ListActivity.java
 |
 |   Copyright (c) AppFactory. All rights reserved.
 **************************************************************************************************/
public class UserLikedPage {

    //------------------------------- CLASS VARIABLES --------------------------------------------//
    private String name;
    private String category;
    private String id;

    //---------------------------- CLASS CONSTRUCTORS --------------------------------------------//

    public UserLikedPage(){
        this.name = "";
        this.category ="";
        this.id="";
    }

    public UserLikedPage(String pageName, String pageCategory, String pageID) {

        this.name = pageName;
        this.category = pageCategory;
        this.id = pageID;

    }

    //---------------------------- CLASS ACCESSORS -----------------------------------------------//

    public String getName(){
        return this.name;
    }

    public String getCategory(){
        return this.category;
    }

    public String getID(){
        return this.id;
    }


    //---------------------------- CLASS MUTATORS ------------------------------------------------//

    public void setName(String pageName){
        this.name = pageName;
    }

    public void setCategory (String pageCategory){
        this.category = pageCategory;
    }

    public void setID (String pageID){
        this.id = pageID;
    }

}
