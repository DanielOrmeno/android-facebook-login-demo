package com.dorlomin.facebookdemo;

/***************************************************************************************************
 |   File: ListAdapter.java
 |   Proyect: Android - Facebook Login Demo
 |
 |   Description: - Custom adapter class (extends ArrayAdapter), used to populate custom layout on
 |   user's liked facebook page view list.
 |
 |   Copyright (c) AppFactory. All rights reserved.
 **************************************************************************************************/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by danielormeno on 9/04/15.
 */
public class ListAdapter extends ArrayAdapter <UserLikedPage>{

    //------------------------------- CLASS VARIABLES --------------------------------------------//

    //- List of users liked pages
    List<UserLikedPage> pageList;

    //- Field TAGS
    private static String FIELD_TAG = "picture";

    //------------------------------- CLASS CONSTRUCTORS -----------------------------------------//

    public ListAdapter (Context context, ArrayList<UserLikedPage> list) {

        super(context, R.layout.liked_page, list);

        pageList = list;
    }

    //------------------------------- CLASS METHODS ----------------------------------------------//

    @Override
    public int getCount() {
        return pageList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.liked_page, null);
        }

        UserLikedPage page = pageList.get(position);

        if(page != null) {

            final ImageView icon = (ImageView)v.findViewById(R.id.page_icon);
            TextView name = (TextView) v.findViewById(R.id.page_title);
            TextView category = (TextView)v.findViewById(R.id.page_category);

            //- Set TextView Values
            name.setText(page.getName());
            category.setText(page.getCategory());

            //- Get Images through Facebook API
            GraphRequest request = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    page.getID(),
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {

                            try {
                                //- get JSONObject from GraphResponse
                                JSONObject graphImage = response.getJSONObject().getJSONObject("picture");
                                //- Get Page Profile Picture
                                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);

                                URL imgUrl = new URL(graphImage.getJSONObject("data").getString("url"));

                                InputStream in = (InputStream) imgUrl.getContent();
                                Bitmap bitmap = BitmapFactory.decodeStream(in);

                                icon.setImageBitmap(bitmap);

                            } catch (JSONException e ) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
            );

            Bundle parameters = new Bundle();
            parameters.putString("fields",FIELD_TAG);
            request.setParameters(parameters);
            //- Perform API call asynchronously
            request.executeAsync();


        }
        return v;
    }
}