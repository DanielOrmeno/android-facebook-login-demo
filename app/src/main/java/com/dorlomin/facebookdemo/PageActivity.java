package com.dorlomin.facebookdemo;
/***************************************************************************************************
 |   File: PageActivity.java
 |   Proyect: Android - Facebook Login Demo
 |
 |   Description: - Displays information relevant to the selected page.
 |
 |   Copyright (c) AppFactory. All rights reserved.
 **************************************************************************************************/

import android.support.v4.app.NavUtils;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;


public class PageActivity extends ActionBarActivity {

    //------------------------------- CLASS VARIABLES --------------------------------------------//

    //- Reference to UI elements
    ImageView pagePicture;
    TextView pageName;
    TextView pageCategory;
    TextView pageDescription;

    //- Intent TAGS
    private static String NAME_TAG = "name";
    private static String CAT_TAG = "cat";
    private static String ID_TAG = "id";

    //- Field TAGS
    private static String FIELD_TAG = "picture, about";
    private static String ABOUT_TAG = "about";
    private static String PICTURE_TAG = "picture";

    //- Page ID
    String pageID;


    //------------------------------- ACTIVITY SPECIFIC METHODS ----------------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_page);

        pageName = (TextView) findViewById(R.id.page_title_text);
        pageCategory = (TextView) findViewById(R.id.page_cat);
        pagePicture = (ImageView) findViewById(R.id.page_profile_image);
        pageDescription = (TextView) findViewById(R.id.page_desc);

        if (savedInstanceState == null) {
            Intent in = getIntent();
            Bundle intentExtras = getIntent().getExtras();

            if (intentExtras != null) {

                pageName.setText(intentExtras.getString(NAME_TAG));
                pageCategory.setText(intentExtras.getString(CAT_TAG));
                pageID = intentExtras.getString(ID_TAG);

                //- Make API Call
                requestPageData (AccessToken.getCurrentAccessToken(), pageID);

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //------------------------------- CLASS METHODS ----------------------------------------------//

    private void requestPageData ( AccessToken accessToken, String pageId) {

        //- Graph API Call

        GraphRequest request = new GraphRequest(
                accessToken,
                pageId,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        try {
                            //- get JSONObject from GraphResponse
                            JSONObject graphImage = response.getJSONObject().getJSONObject("picture");
                            JSONObject graphAbout = response.getJSONObject();

                            //- Get Page Profile Picture
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);

                            //URL imgUrl = new URL("http://graph.facebook.com/"+pageID+"/picture?type=large");
                            URL imgUrl = new URL(graphImage.getJSONObject("data").getString("url"));

                            InputStream in = (InputStream) imgUrl.getContent();
                            Bitmap  bitmap = BitmapFactory.decodeStream(in);

                            //- Set Page Image
                            pagePicture.setImageBitmap(bitmap);
                            //- Update UI
                            pageDescription.setText(graphAbout.getString(ABOUT_TAG));


                        } catch (JSONException e ) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields",FIELD_TAG);
        request.setParameters(parameters);
        //- Perform API call asynchronously
        request.executeAsync();
    }

}
