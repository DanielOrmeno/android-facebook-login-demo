package com.dorlomin.facebookdemo;
/***************************************************************************************************
 |   File: ListActivity.java
 |   Proyect: Android - Facebook Login Demo
 |
 |   Description: - Lists all user's liked pages, uses the custom class ListAdapter to populate a
 |   custom layout for displaying page information.
 |
 |   Copyright (c) AppFactory. All rights reserved.
 **************************************************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ListActivity extends ActionBarActivity {

    //------------------------------- CLASS VARIABLES --------------------------------------------//
    private CallbackManager callbackManager;
    private JSONObject dataObject;
    private JSONArray dataArray;

    //- Intent TAGS
    private static String LIKEDPAGES_TAG = "LIKEDPAGES";
    private static String NAME_TAG = "name";
    private static String CAT_TAG = "cat";
    private static String ID_TAG = "id";

    //- List of Liked pages
    List <UserLikedPage> likedPages;

    //- Reference to UI elements
    ListView pageList;
    private LoginButton facebookLoginButton;

    //------------------------------- ACTIVITY SPECIFIC METHODS ----------------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //- Initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        //- Callback Manager
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_list);

        //- Instantiate likedPages list
        likedPages = new ArrayList <UserLikedPage>();

        //- Find List View
        pageList = (ListView) findViewById(R.id.pages_list);

        //- Facebook Login UI Elements
        facebookLoginButton = (LoginButton) findViewById(R.id.login_button);
        facebookLoginButton.setReadPermissions("user_likes");

        if (savedInstanceState == null) {
            Intent in = getIntent();
            Bundle intentExtras = getIntent().getExtras();

            if (intentExtras != null) {

                try {

                    //- Create JSONObject from dataObject
                    dataObject = new JSONObject(intentExtras.get(LIKEDPAGES_TAG).toString());
                    //- populate array
                    dataArray = dataObject.getJSONArray("data");

                    for (int i=0; i<dataArray.length(); i++){
                        JSONObject likedPage = dataArray.getJSONObject(i);

                        String name = likedPage.getString("name");
                        String category = likedPage.getString("category");
                        String id = likedPage.getString("id");

                        //- UserLikedPage Object to parse into list
                        likedPages.add(new UserLikedPage(name, category, id));
                    }

                } catch (JSONException e ) {
                    e.printStackTrace();
                }

                //- If there are results, populate view
                if (!likedPages.isEmpty()){
                    final ListAdapter adapter = new ListAdapter(this.getApplicationContext(), (ArrayList<UserLikedPage>)likedPages);
                    pageList.setAdapter(adapter);

                    pageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                            UserLikedPage page = (UserLikedPage) adapter.getItem(position);

                            //- Setup Intent
                            Intent in = new Intent (ListActivity.this, PageActivity.class);
                            in.putExtra(NAME_TAG, page.getName());
                            in.putExtra(CAT_TAG, page.getCategory());
                            in.putExtra(ID_TAG, page.getID());

                            //- Start new Activity with relevant data.
                            startActivity(in);
                        }

                    });
                }

            }
        }

        //- Facebook Login Callback declaration
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                startActivity(new Intent (ListActivity.this, loginActivity.class));
                Log.i("Facebook", "Login/Logout Successful");
            }

            @Override
            public void onCancel() {
                Log.i("Facebook", "Login/Logout Canceled");
                startActivity(new Intent (ListActivity.this, loginActivity.class));
            }

            @Override
            public void onError(FacebookException e) {
                Log.i("Facebook", "Login/Logout Error");
                startActivity(new Intent (ListActivity.this, loginActivity.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
